<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    protected $uid;

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model("User_model", "users");
    }

    public function list($id=0){
        $resps = [];
        if(empty($id)){
            $page = (!empty($this->input->get("page"))) ? $this->input->get("page") : 1;
            $rpp = (!empty($this->input->get("rpp"))) ? $this->input->get("rpp") : 6;
            $sel = "id, first_name, last_name, email, IFNULL(avatar, '') as avatar";
            $userdata = $this->users->lists(null, $sel, $rpp, $page);
            $totaldata = $this->users->total();
            $total_page = ceil($totaldata / $rpp);
            $current_page = $page;
            $next_page = ($page < $total_page) ? $page + 1 : $total_page;
            $prev_page = ($page > 1) ? $page - 1 : 1; 
            $resps = [
                'page'=>$page,
                'per_page'=>$rpp,
                'total'=>$totaldata,
                'total_pages'=>$total_page,
                'data'=>$userdata,
                'support'=>[
                    "url"=>"https://reqres.in/#support-heading",
                    "text"=>"To keep ReqRes free, contributions towards server costs are appreciated!"
                ] 
            ];
        }else{
            $sel = "id, first_name, last_name, email, IFNULL(avatar, '') as avatar";
            $user = $this->users->findCond(['id'=>$id], $sel);
            if(!empty($user)){
                $resps = [
                    'data'=>$user,
                    'support'=>[
                        "url"=>"https://reqres.in/#support-heading",
                        "text"=>"To keep ReqRes free, contributions towards server costs are appreciated!"
                    ] 
                ];
            }else{
                $resps = (object)$resps;
            }
        }
        header("Accept-Type: application/json");
        header("Content-Type: application/json");
        echo json_encode($resps, JSON_UNESCAPED_SLASHES);
    }
}
