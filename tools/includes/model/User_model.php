<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class User_model extends CI_Model{

			protected $table;

			function __construct(){
				parent::__construct();
				$this->table = 'user';
			}

			function lists($cond=null, $select='', $rpp=6, $page=1){
				if(!empty($cond)) $this->db->where($cond);
				if(!empty($page) && $page != 'all'){
					$spage = ($page > 1) ? ($page - 1) * $rpp : 0;
					if(!empty($rpp) && $rpp != 'all'){ 
						$this->db->limit($rpp, $spage);
					}
				}
				if(!empty($select)) $this->db->select($select);
				$sql = $this->db->get($this->table);
				$data = ($sql && $sql->num_rows() > 0) ? $sql->result() : [];
				return $data;
			}

			function total($cond=null){
				if(!empty($cond)) $this->db->where($cond);
				$sql = $this->db->select("COUNT(id) as total")->get($this->table);
				$total = ($sql && $sql->num_rows() > 0) ? (int)$sql->row()->total : 0;
				return $total;
			}

			function insert($data){
				$this->db->insert($this->table, $data);
				return $this->db->insert_id();
			}

			function update($data, $cond){
				if(!is_array($cond)){
					$cond = array('id'=>$cond);
				}
				return $this->db->update($this->table, $data, $cond);
			}

			function findCond($where, $select='') {
				if(!empty($select)) $this->db->select($select);
				$usdata = $this->db->get_where($this->table, $where);
				return $usdata->row();
			}
}
