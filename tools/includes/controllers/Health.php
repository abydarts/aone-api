<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Health extends CI_Controller {

  public function __construct() {
      parent::__construct();
      // load base_url
      $this->load->helper('url');
  }

  public function index(){
      headaer('Content-Type: application/json');
      echo json_encode(['code'=>0, 'message'=>'ok']);
  }

}
